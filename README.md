# CesTip BE Python

Python backend application which is part of B6B36RSP project.

# IMPORTANT

Before running the application make sure you have **python 3.8+** installed!

`python --version`

To install the libraries type these commands in cmd.exe:

`pip3 install Flask` (version 1.1.2)

`pip3 install Flask-RESTful`

`pip3 install py_eureka_client`

To check flask version:

`python -m flask --version`

To run the application:

`cd .\src\main\`

`python .\api.py`

(if you have Linux, find the commands for yourself :) ).