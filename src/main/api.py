from flask import Flask
from flask_restful import Resource, Api, reqparse
import py_eureka_client.eureka_client as eureka_client
from datetime import datetime, timedelta
import urllib
import sys

from service import statistics

rest_server_port = 5000
# The folowing code will register your server to eureka server and also start to send heartbeat every 30 seconds
eureka_client.init(eureka_server = "http://cestip.tk:8761/eureka/",
                   app_name = "statisticsService",
                   instance_port = rest_server_port)

app = Flask(__name__)
api = Api(app)

parser = reqparse.RequestParser()

class Evolution(Resource):

    def get(self, type):
        parser.add_argument("from")
        parser.add_argument("to")
        parser.add_argument("destination")
        parser.add_argument("accomodationType")
        parser.add_argument("boarding")
        parser.add_argument("adults")
        parser.add_argument("kids")
        parser.add_argument("minPrice")
        parser.add_argument("maxPrice")
        args = parser.parse_args()

        args["downloadDateFrom"] = (datetime.now() - timedelta(days=7)).strftime('%Y-%m-%d')
        args["downloadDateTo"] = datetime.today().strftime('%Y-%m-%d')

        """
        if args["from"] is None:
            args["from"] = (datetime.now() - timedelta(days=30)).strftime('%Y-%m-%d')

        if args["to"] is None:
            args["to"] = (datetime.today() + timedelta(days=30)).strftime('%Y-%m-%d')
        """

        try:
            if type == "avg":
                return statistics.average_prices_evo(args)
            elif type == "min":
                return statistics.min_prices_evo(args)
            elif type == "max":
                return statistics.max_prices_evo(args)
            else:
                return "Not Found", 404

        except urllib.request.HTTPError as e:
            # If all nodes are down, an `HTTPError` will be raised
            print("Service Unavailable")
            return "Service Unavailable", 503

api.add_resource(Evolution, '/prices/<type>/evolution')

class Prices(Resource):

    def get(self, type):
        parser.add_argument("from")
        parser.add_argument("to")
        parser.add_argument("destination")
        parser.add_argument("accomodationType")
        parser.add_argument("boarding")
        parser.add_argument("adults")
        parser.add_argument("kids")
        parser.add_argument("minPrice")
        parser.add_argument("maxPrice")
        args = parser.parse_args()

        args["downloadDateFrom"] = (datetime.now() - timedelta(days=7)).strftime('%Y-%m-%d')
        args["downloadDateTo"] = datetime.today().strftime('%Y-%m-%d')

        """
        if args["from"] is None:
            args["from"] = (datetime.now() - timedelta(days=7)).strftime('%Y-%m-%d')
        if args["to"] is None:
            args["to"] = datetime.today().strftime('%Y-%m-%d')
        """

        try:
            if type == "avg":
                return statistics.average_prices(args)
            elif type == "min":
                return statistics.min_prices(args)
            elif type == "max":
                return statistics.max_prices(args)
            else:
                return "Not Found", 404

        except urllib.request.HTTPError as e:
            # If all nodes are down, an `HTTPError` will be raised
            print("Service Unavailable")
            return "Service Unavailable", 503

api.add_resource(Prices, '/prices/<type>')

class TermCount(Resource):

    def get(self):
        parser.add_argument("from")
        parser.add_argument("to")
        parser.add_argument("destination")
        parser.add_argument("accomodationType")
        parser.add_argument("boarding")
        parser.add_argument("adults")
        parser.add_argument("kids")
        parser.add_argument("minPrice")
        parser.add_argument("maxPrice")
        args = parser.parse_args()

        args["downloadDateFrom"] = (datetime.now() - timedelta(days=7)).strftime('%Y-%m-%d')
        args["downloadDateTo"] = datetime.today().strftime('%Y-%m-%d')

        try:
            return statistics.count_terms(args)

        except urllib.request.HTTPError as e:
            # If all nodes are down, an `HTTPError` will be raised
            print("Service Unavailable")
            return "Service Unavailable", 503

api.add_resource(TermCount, '/term-count')

# this must be at the end of the file
if __name__ == "__main__":
    if len(sys.argv) > 1:
        app.run(debug=False, host='0.0.0.0')
    else:
        app.run(debug=True)
